
# coding: utf-8

# Domain Adaptation Baseline Test 2+3 for new PTFr and CRTS data (may also include LINEAR)
# * no sampling methods, neither SMOTE nor under as we have ample data!
# * select 400~500 from each class from each survey to form the domain data
# * eleminate part of the source data in training which has the same ID as the cross-validation data
# * save 10% objects from each class for testing
# * Use K-fold cross-validation to test for base results
# * plot with error bar

# In[1]:
<<<<<<< HEAD

=======
>>>>>>> 2894a389cb15b5281b7ceec03548a785c476ebb1
from __future__ import division
import pandas as pd
import numpy as np
from pandas import DataFrame
from sklearn import cross_validation as cv
from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import LinearSVC
from scipy.stats import sem
import matplotlib.pyplot as plt
<<<<<<< HEAD
=======
from matplotlib import colors
from matplotlib.pyplot import setp
>>>>>>> 2894a389cb15b5281b7ceec03548a785c476ebb1
from matplotlib.lines import Line2D
from sklearn.decomposition import PCA
from sklearn.cluster import KMeans 
from collections import Counter
<<<<<<< HEAD
=======
from unbalanced_dataset import SMOTE
from sklearn.metrics import confusion_matrix
from itertools import compress
>>>>>>> 2894a389cb15b5281b7ceec03548a785c476ebb1
import pickle


# In[2]:

#creat labels for data
def add_Names(filename, selected_features, ):
    # Start with an empty dictionary
    stats = {}
    for line in open(filename):
        # Split the config.dat file with delimiter ','; key is the feature number and value is feature name
        line = line.replace("'", "")
        temp = line.rstrip().split(',')
        stats[temp[0]] = temp[1]
    features = []
    for feature in selected_features:
        features.append(stats[str(feature)])
    features.append(class_label)
    return features

#Select data based on objects' labels (subset for certain classes)
def selectdata(data, Selected_Class):
    dataF = pd.DataFrame()
    i = 0
    for c in Selected_Class:
        dataF =  dataF.append(data[data[class_label] == c])
        #dataF.loc[data[class_label] == c,class_label] = i
        i+=1
    return dataF

def test_train_label(X, split, offset):
    len_feature = len(X.columns) - offset
    train, test = cv.train_test_split(X, train_size=(split/100))
    X_train = train.ix[:,range(0,len_feature)]
    y_train= train[class_label]
    X_test = test.ix[:,range(0,len_feature)]
    y_test = test[class_label]
    return X_train,y_train,X_test,y_test

def normalize_data_with_label(data):
    df = data.ix[:,range(0,len_feature)]
    data_norm = (df - df.mean()) / (df.max() - df.min())
    data_norm[class_label] = data[class_label]
    return data_norm

#minus mean and divided by standard deviation
def normalize_data_with_label2(data):
    df = data.ix[:,range(0,len_feature)]
    data_norm = (df - df.mean()) / df.std()
    data_norm[class_label] = data[class_label]
    return data_norm

def sample_wo_replacement(data, size, Selected_Class):
    o_data = pd.DataFrame()
    for c in Selected_Class:
        temp = data[data[class_label] == c]
        class_size = len(temp)
        if size > class_size:
            indexes = np.random.choice(temp.index, size-class_size, replace=True)
            temp = temp.append(temp.ix[indexes])
        else:
            indexes = np.random.choice(temp.index, size, replace=False)
            temp = temp.ix[indexes]
        o_data = o_data.append(temp)
    return o_data

def sample_wo_replacement_by_ratio(data, ratio, Selected_Class):
    o_data = pd.DataFrame()
    for c in Selected_Class:
        temp = data[data[class_label] == c]
        class_size = len(temp)    
        if ratio > 1:
            indexes = np.random.choice(temp.index, class_size*(ratio-1), replace=True)
            temp = temp.append(temp.ix[indexes])
        else:
            indexes = np.random.choice(temp.index, class_size*ratio, replace=False)
            temp = temp.ix[indexes]
        o_data = o_data.append(temp)
    return o_data

def plot_confusion_matrix(cm, title, Selected_Class, cmap=plt.cm.Blues):
    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(Selected_Class))
    plt.xticks(tick_marks, Selected_Class, rotation=45)
    plt.yticks(tick_marks, Selected_Class)
    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    
def plot_feature_dist(feature, title):
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(Selected_Class))
    plt.xticks(tick_marks, Selected_Class, rotation=45)
    plt.yticks(tick_marks, Selected_Class)
    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    
# function for setting the colors of the box plots pairs
def setBoxColors(bp, colors):
    gap1 = 0
    gap2 = 0
    for c in colors:
        setp(bp['boxes'][gap1], color=c)
        setp(bp['caps'][gap2], color=c)
        setp(bp['caps'][gap2+1], color=c)
        setp(bp['whiskers'][gap2], color=c)
        setp(bp['whiskers'][gap2+1], color=c)
        setp(bp['fliers'][gap2], color=c)
        setp(bp['fliers'][gap2+1], color=c)
        setp(bp['medians'][gap1], color=c)
        gap1 = gap1 + 1
        gap2 = gap2 + 2


# In[3]:

def K_fold_cross_validation(K_fold):
    Y_Tests = []
    Y_Preds = []
    print "%d_fold cross validation: " %K_fold
    for data, name in [(crts,"crts"), (ptfr,"ptfr"), (lineardb,"lineardb")]:
        Y_test = []
        Y_pred = []
        Indexes = []
        test_smaller_class = len(data[data[class_label]==6])
        for c in Selected_Class:
            temp =  data[data[class_label] == c]
            len_size = test_smaller_class//K_fold #len(temp) or to get a balanced data: test_smaller_class 
            chunks = []
            for i in range(0, K_fold):   
                rows = temp.sample(len_size).index.values
                chunks.append(rows)
                temp = temp.drop(rows)
            Indexes.append(chunks)

        #Every row in Indexes is the data for class c that are splited into K chuncks
        class_index = []
        for i in range(0, K_fold):
            indexes_split = []
            for c in range(0,len(Indexes)):
                indexes_split.extend(Indexes[c][i])
            class_index.append(indexes_split)

        #K_fold cross validation
        scores = []
        for class_for_test in range(0, K_fold):
            temp_index = []
            for i in range(0, K_fold):
                if(i is not class_for_test):
                    temp_index.extend(class_index[i])
            train = data[data.index.isin(temp_index)]
            test = data[data.index.isin(class_index[class_for_test])]
            rfc.fit(train.ix[:,range(0,len_feature)], train[class_label])
            Y_pred.extend(rfc.predict(test.ix[:,range(0,len_feature)]))
            Y_test.extend(test[class_label])        
            scores.append(rfc.score(test.ix[:,range(0,len_feature)], test[class_label]))
        print "average accuracy for %s is %f" % (name,np.mean(scores))
        Y_Tests.append(Y_test)       
        Y_Preds.append(Y_pred)
    return Y_Tests, Y_Preds


# method find_good_features:
# * outut format:
# * class_x, class_pred_as_x, feature name, s_name, t_name, overlap
# * Note: 0 means no overlap while 1 means less than 5% overlap
# e.g. 6, 2, amplitude, lineardb, crts, 1
# * which means EA objects which are predicted as RRd could be distinguished from objetcs that are real RRd by feature amplitude with less than 5% overlap during lineardb-to-crts

# In[4]:

def find_good_features(feature, s_name, t_name, class_order, feature_box):
    correct_data = feature_box[0]
    real_class = class_order[0]
    real_upper_bound = correct_data.mean() + correct_data.std()
    real_lower_bound = correct_data.mean() - correct_data.std()
    output = []
    for i in range(1, len(feature_box)):
        mispred_data = feature_box[i]
        pred_upper_bound = mispred_data.mean() + mispred_data.std()
        pred_lower_bound = mispred_data.mean() - mispred_data.std()
        if((pred_lower_bound > real_upper_bound) or (pred_upper_bound < real_lower_bound)):
            #pass direcly
            output.append([real_class, class_order[i], feature, s_name, t_name, 0])
        #pred is higher than real
        elif(mispred_data.mean() > correct_data.mean):
            if (((real_upper_bound - pred_lower_bound)/(pred_upper_bound - real_lower_bound)) < 0.05):
                #still pass with small overlap
                output.append([real_class, class_order[i], feature, s_name, t_name, 1])
        #pred is lower than real
        elif(mispred_data.mean() < correct_data.mean):
            if (((real_lower_bound - pred_upper_bound)/(real_upper_bound - pred_lower_bound)) < 0.05):
                #still pass with small overlap
                output.append([real_class, class_order[i], feature, s_name, t_name, 1])
    return output

def interpret_output(Output):
    for output in Output:
        class_x = inverse_classes[str(output[0])]
        class_pred_as_x = inverse_classes[str(output[1])]
        overlap = ''
        if output[5] == 1:
            overlap = 'less than 5% overlap'
        elif output[5] == 0:
            overlap = 'no overlap'
        print '%s objects which are predicted as %s could be distinguished '         'from objetcs that are real %s by feature %s with %s during '         '%s-to-%s' %(class_pred_as_x, class_x, class_x, output[2], overlap, output[3], output[4])


# In[5]:

def findSmallestClassSize(data, Selected_Class):
    smallest_size = 9999999999
    for c in Selected_Class:
        temp = data[data[class_label] == c]
        class_size = len(temp)
        if smallest_size > class_size:
            smallest_size = class_size
    return smallest_size


# In[6]:

selected_features = list(range(1,22))
selected_features.remove(15)
len_feature = len(selected_features)
filename = 'config.dat'
class_label = "class"
labels = add_Names(filename,selected_features)


# In[7]:

# Start with an empty dictionary
classes = {}
for line in open('Class_list'):
    # Split the config.dat file with delimiter ','; key is the feature number and value is feature name
    line = line.replace("'", "")
    temp = line.rstrip().split('\t')
    classes[temp[0]] = temp[3]
#still eliminate 'LPV' since it contains less than 100 objects in PTFr
Selected_Class_names = ["EW","EA","RRab","RRc","RRd","RS CVn"]  
Selected_Class = [int(classes[x]) for x in Selected_Class_names]
inverse_classes = dict(zip(classes.values(), classes.keys()))


# In[8]:

#constant value
start_size = 5
end_size = 55
bins = 5
<<<<<<< HEAD
iter_time1 = 30
iter_time2 = 30

=======
iter_time1 = 10
iter_time2 = 10
>>>>>>> 2894a389cb15b5281b7ceec03548a785c476ebb1


# In[9]:

svc = LinearSVC(class_weight='auto')
rfc = RandomForestClassifier(class_weight='auto')


<<<<<<< HEAD
norm = True
balance = False
dim_red = False


# In[15]:
=======
# In[11]:
>>>>>>> 2894a389cb15b5281b7ceec03548a785c476ebb1

csdr2 = pd.read_csv('CSDR2_lc_data.csv')
ptfr = pd.read_csv('R_PTF_lc_features.csv')
lineardb = pd.read_csv('Linear_lc_over40.csv')
<<<<<<< HEAD
test_smaller_class = len(lineardb[lineardb[class_label]==8]) #which is 330 or 300

# In[20]:

#Select only common IDs for those surveys
csdr2_comm = csdr2[csdr2.index.isin(lineardb.index.values)]

# In[11]:

#Not using only common data

if norm:
    csdr2 = csdr2.dropna()
    ptfr = ptfr.dropna()
    lineardb = lineardb.dropna()
    csdr2 = normalize_data_with_label2(csdr2)
    ptfr = normalize_data_with_label2(ptfr)
    lineardb = normalize_data_with_label2(lineardb)
if balance:
    Selected_Class = [1,2,4,5,8]
    csdr2_b = sample_wo_replacement(csdr2, 500)
if dim_red:
    X = normalize_data_with_label2(csdr2).iloc[:,range(0,len_feature)]
    pca = PCA().fit(X)
    total_var = 0
    var_ratio = pca.explained_variance_ratio_
    for i in range(0, len(var_ratio)):
        total_var = total_var+var_ratio[i]
        if (total_var >= 0.9):
            break
    k_components = i+1
    csdr2_p = pd.DataFrame(PCA(n_components=k_components).fit_transform(X), index=csdr2.index.values)
  
# In[17]:

print ("Class distribution among these surveys")
print ("CSDR2: ", Counter(csdr2[class_label]))
print ("PTFr: ", Counter(ptfr[class_label]))
print ("lineardb: ", Counter(lineardb[class_label]))
print ("common: ", Counter((csdr2[csdr2.index.isin(lineardb.index.values)])[class_label]))
=======
test_smaller_class = len(ptfr[ptfr[class_label]==6]) 
crts = csdr2


# In[12]:

print "Class distribution among these surveys"
print "CSDR2: ", Counter(csdr2[class_label])
print "PTFr: ", Counter(ptfr[class_label])
print "lineardb: ", Counter(lineardb[class_label])
print "common csdr_linear: ", Counter((csdr2[csdr2.index.isin(lineardb.index.values)])[class_label])
print "common csdr_ptfr: ", Counter((csdr2[csdr2.index.isin(ptfr.index.values)])[class_label])
csdr_linear = csdr2[csdr2.index.isin(lineardb.index.values)]
csdr_ptfr = csdr2[csdr2.index.isin(ptfr.index.values)]

# In[14]:
>>>>>>> 2894a389cb15b5281b7ceec03548a785c476ebb1

#modified domain adaptaion method that uses balanced source data for traianing 
#and use exact same num of target data for S+T-to-T and T-to-T
def Domain_Adaptation23_modified(S_data, T_data, Clf, target_smaller_class, source_smaller_class, checkBoth):
    Learning_accuracies = []
    Feature_importance = []
    Learning_accuracies_TT = []
    Feature_importance_TT = []
    #select n number of data from each class in target domain for cross-validation
    Y_tests = []
    Y_preds = []
    Y_preds_TT = []
    Y_indexes = []
    for split in range(start_size,end_size,bins):
        Y_test = []
        Y_pred = []
        Y_pred_TT = []
        Y_index = []
        learn_accuracy = []
        learn_accuracy_TT = []
        importances = []
        importances_TT = []
        #select size objects from each class for training
        size = ((split/100)*target_smaller_class)
        for i in range(0, 10):
            Source_train = sample_wo_replacement(S_data, source_smaller_class, Selected_Class)
            #perform iter_time1 selecting Target_train
            for j in range(0,iter_time1):
                scores = []
                scores_TT = []
                #size: the size of a single class, so in total, target data for training will be size*num_of_classes
                Target_train = sample_wo_replacement(T_data, size, Selected_Class)         
                #perform iter_time2 slecting Test_target
                for k in range(0,iter_time2):
                    #include objects that are in the class
                    Test_target = (T_data[~T_data.index.isin(Target_train.index.values)]).sample(frac=0.1)
                    X_test = np.array(Test_target.ix[:,range(0,len_feature)])
                    y_test = Test_target[class_label].values
                    y_index = Test_target.index.values
                    
                    Train_data = Target_train.append(Source_train)
                    X_train = np.array(Train_data.ix[:,range(0,len_feature)])
                    y_train = Train_data[class_label].values
                    Clf.fit(X_train, y_train)
                    scores.append(Clf.score(X_test, y_test))
                    Y_test.extend(y_test)
                    Y_pred.extend(Clf.predict(X_test))
                    Y_index.extend(y_index)
                    if (type(Clf) == type(rfc)):
                        importances.append(Clf.feature_importances_)
                    if checkBoth:
                        X_train = np.array(Target_train.ix[:,range(0,len_feature)])
                        y_train = Target_train[class_label].values
                        #y_train = y_train.astype(int)
                        Clf.fit(X_train, y_train)
                        scores_TT.append(Clf.score(X_test, y_test))
                        Y_pred_TT.extend(Clf.predict(X_test))
                        if (type(Clf) == type(rfc)):
                            importances_TT.append(Clf.feature_importances_)
                learn_accuracy.append(np.mean(scores))
                learn_accuracy_TT.append(np.mean(scores_TT))
        if (type(Clf) == type(rfc)):
            ave_f_importance =  np.mean(np.array(importances), axis=0)
            Feature_importance.append(ave_f_importance)
            ave_f_importance_TT =  np.mean(np.array(importances_TT), axis=0)
            Feature_importance_TT.append(ave_f_importance_TT)
        Learning_accuracies.append(np.mean(learn_accuracy))
        Y_tests.append(Y_test)
        Learning_accuracies_TT.append(np.mean(learn_accuracy_TT))
        Y_preds_TT.append(Y_pred_TT)
        Y_preds.append(Y_pred)
        Y_indexes.append(Y_index)
    return Learning_accuracies, Learning_accuracies_TT, Clf, Feature_importance, Feature_importance_TT, Y_tests, Y_preds, Y_preds_TT, Y_indexes


# In[16]:

<<<<<<< HEAD
fig = plt.figure(figsize=(15,10))
ax = fig.add_subplot(111)
=======
'''Domain Adaptation learning curve for T-2-T and S+T-2-T'''
>>>>>>> 2894a389cb15b5281b7ceec03548a785c476ebb1
runs = 4
Learning_curves = []
labels = []
Y_Tests = []
Y_Preds_TT = []
Y_Preds = []
Y_indexes = []
for run in range(0,runs):
    '''Domain Adaptation Baseline Test 2 (S+some T) to Target'''
    print "run %d" %run
    Learning_curve = []
<<<<<<< HEAD
    for clf, name, S_data, T_data in [(rfc, 'CSDR-2-Lineardb', csdr2, lineardb), (rfc, 'PTFr-2-Lineardb', ptfr, lineardb), ]:
        Learning_accuracies, clf, Feature_importance = Domain_Adaptation2_modified(S_data, T_data, clf)
=======
    clf = rfc
    for S_data, T_data, s_name, t_name, checkBoth in [(lineardb, crts, "Lineardb", "CRTS", True),
                                                      (crts, ptfr, "CRTS", "PTF(r)", True),
                                                      (ptfr, crts, "PTF(r)", "CRTS", False),
                                                      (crts, lineardb, "CRTS", "Lineardb", True), 
                                                      (ptfr, lineardb, "PTF(r)", "Lineardb", False), 
                                                      (lineardb, ptfr, "Lineardb", "PTF(r)",False)]:
        S_data = normalize_data_with_label2(S_data)
        T_data = normalize_data_with_label2(T_data)
        target_smaller_class = findSmallestClassSize(T_data, Selected_Class)
        source_smaller_class = findSmallestClassSize(S_data, Selected_Class)
        Learning_accuracies, Learning_accuracies_TT, Clf, Feature_importance,         Feature_importance_TT, Y_tests, Y_preds, Y_preds_TT, Y_index = Domain_Adaptation23_modified(
            S_data, T_data, clf, target_smaller_class, source_smaller_class, checkBoth)
>>>>>>> 2894a389cb15b5281b7ceec03548a785c476ebb1
        Learning_curve.append(Learning_accuracies)
        labels.append("S+T to T %s-2-%s" % (s_name,t_name))
        pickle.dump([Learning_accuracies, Feature_importance], open("S+T to T %s-2-%s.p" % (s_name,t_name), "w" ))
        print "pickle dump for %s-2-%s" % (s_name,t_name)
        if checkBoth:
            Learning_curve.append(Learning_accuracies_TT)
            labels.append("T to T %s" % t_name)
            pickle.dump([Learning_accuracies_TT, Feature_importance_TT], open("T to T %s.p" % t_name, "w" ))
            print "pickle dump for %s" % t_name
            Y_Preds_TT.append(Y_preds_TT)
        Y_Tests.append(Y_tests)
        Y_Preds.append(Y_preds)
        Y_indexes.append(Y_index)   
    Learning_curves.append(np.array(Learning_curve))
pickle.dump([Learning_curves, labels], open("Learning_curves_labels.p" % t_name, "w" ))


<<<<<<< HEAD
    for clf, name, data in [(rfc, 'lineardb', lineardb)]:    
        Learning_accuracies, clf, Feature_importance = Domain_Adaptation2_modified(pd.DataFrame(), data, clf)
        Learning_curve.append(Learning_accuracies)
        labels.append("T to T %s" % name)
        '''
        ax.plot(range(start_size,end_size,bins), Learning_accuracies, "s-",
                 label="T to T %s" % (name, ), marker = Line2D.filled_markers[index])
        index += 1
        '''
    Learning_curves.append(np.array(Learning_curve))
=======
# In[17]:
>>>>>>> 2894a389cb15b5281b7ceec03548a785c476ebb1

fig = plt.figure(figsize=(18.75,10))
ax = fig.add_subplot(111)
curve_plot = np.mean(Learning_curves,axis=0)
std_err = sem(Learning_curves)
pickle.dump(curve_plot, open('learning_curves_linear_as_target.p','wb'))
for index in range(0, curve_plot.shape[0]):
    #ax.plot(range(start_size,end_size,bins), Learning_accuracies, "s-",
    #            label=labels[index],marker = Line2D.filled_markers[index])
    ax.errorbar(range(start_size,end_size,bins), curve_plot[index], yerr=std_err[index], 
                label=labels[index], marker = Line2D.filled_markers[index])
    
<<<<<<< HEAD
ax.set_title("Average acurracies for S+T-to-T and T-to-T (Lineardb as target domain)")
ax.set_xlabel("Target data training percentage %")
ax.set_ylabel("Average accuracy")
=======
ax.set_title("Average acurracies for S+T-to-T and T-to-T (three surveys)",fontsize=24)
ax.set_xlabel("Target data training percentage %",fontsize=18)
ax.set_ylabel("Average accuracy",fontsize=18)
>>>>>>> 2894a389cb15b5281b7ceec03548a785c476ebb1

# Shrink current axis by 20%
box = ax.get_position()
ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])
ax.tick_params(axis='x', labelsize=18)
ax.tick_params(axis='y', labelsize=18)
# Put a legend to the right of the current axis
<<<<<<< HEAD
ax.legend(loc='upper left', bbox_to_anchor=(1, 0.4))
plt.savefig('Lineardb_target_RF_DA2_3.png')



=======
ax.legend(loc='upper left', bbox_to_anchor=(1, 0.6),fontsize=18)
plt.savefig('three_surveys_RF_DA2_3.png')
>>>>>>> 2894a389cb15b5281b7ceec03548a785c476ebb1
